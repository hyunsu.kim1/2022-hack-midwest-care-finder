/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env: { GOOGLE_MAPS_API_KEY: process.env.GOOGLE_MAPS_API_KEY },
  assetPrefix:
    process.env.NODE_ENV === "production"
      ? "/2022-hack-midwest-care-finder"
      : "",
};

module.exports = nextConfig;

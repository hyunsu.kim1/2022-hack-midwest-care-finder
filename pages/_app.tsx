import "../styles/globals.css";
import type { AppProps } from "next/app";
import {
  QueryCache,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { SnackbarProvider } from "notistack";
import { LocationProvider } from "../context/appContext";

const client = new QueryClient({
  queryCache: new QueryCache({
    onError: (error: any, query) => {
      if (query.state.data !== undefined) {
        console.error(error.message);
      }
    },
  }),
});

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <QueryClientProvider client={client}>
      <SnackbarProvider
        maxSnack={3}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        autoHideDuration={3000}
      >
        <LocationProvider
          value={{
            state: "Missouri",
            coordinates: { lat: "39.10039", lng: "-94.584228" },
          }}
        >
          <Component {...pageProps} />
          <ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
        </LocationProvider>
      </SnackbarProvider>
    </QueryClientProvider>
  );
}

export default MyApp;

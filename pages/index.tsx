import type { NextPage } from "next";
import { Grid } from "@mui/material";

import SearchBox from "../components/searchBox";
import { useProxy } from "../services/hooks/use-proxy";
import { useMapProxy } from "../services/hooks/use-map-proxy";
import Map from "../components/map/map";
import { MapResult } from "../type/types";
import { useLocationContext } from "../context/appContext";

const containerStyle = {
  width: "100%",
  height: "100%",
};

const Home: NextPage = () => {
  const { coordinates, state } = useLocationContext();
  console.log(coordinates, state);

  const { data, isLoading } = useMapProxy(
    "abortion_results",
    `abortion&nbsp;clinic`
  );

  const clinicInfos: {
    location: { lat: number; lng: number };
    name: string;
  }[] = data?.results.map((result: MapResult) => {
    return {
      location: {
        lat: result.geometry.location.lat,
        lng: result.geometry.location.lng,
      },
      name: result.name,
    };
  });

  return (
    <Grid container spacing={2} sx={{ height: "100vh" }}>
      <Grid item xs={12} sm={6}>
        <Map
          containerStyle={containerStyle}
          clinicInfos={
            !isLoading
              ? clinicInfos
              : [{ location: { lat: 39.10039, lng: -94.584228 }, name: "" }]
          }
          zoom={10}
        />
      </Grid>
      <Grid item xs={12} sm={6} sx={{ borderLeft: "2px solid black" }}>
        <SearchBox />
      </Grid>
    </Grid>
  );
};

export default Home;

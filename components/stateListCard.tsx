import { Paper, CardContent, ListItemButton, Typography } from "@mui/material";
import { StateRestrictionType } from "../type/types";

interface ListCardProps {
  details: StateRestrictionType;
}

const StateListCard = (props: ListCardProps) => {
  const { details } = props;

  return (
    <Paper elevation={5} sx={{ margin: 1, borderRadius: "25px" }}>
      <ListItemButton
        sx={{
          minWidth: 275,
          borderRadius: "25px",
          mb: 2,
        }}
      >
        <CardContent>
          <>
            <Typography variant="h5" component="div">
              {details.state}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Weeks since last period must be less than:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.weeksSinceLmp}
            </Typography>

            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Waiting period counseling hours required:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.waitingPeriodPostCounselingHours}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Requires insurance coverage:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.requiresCoverage ? "yes" : "no"}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Exception if mother's life in danger:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.privateExceptionLife ? "yes" : "no"}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Counseling visits required:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.counselingVisitCount}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Exception for cases of rape or incest:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.privateExceptionRapeOrIncest ? "yes" : "no"}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Age in which restrictions apply:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.belowAge}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Parent's consent required:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.parentalConsentRequired ? "yes" : "no"}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Allow minor to consent:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.allowsMinorToConsent ? "yes" : "no"}
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              sx={{ fontSize: 14, mt: 1, mb: 1 }}
            >
              Medicaid coverage for "medically necessary" abortions:{" "}
            </Typography>
            <Typography variant="body2" sx={{ fontWeight: "bold" }}>
              {details.medicaidCoverageProviderPatientDecision ? "yes" : "no"}
            </Typography>
          </>
        </CardContent>
      </ListItemButton>
    </Paper>
  );
};

export default StateListCard;

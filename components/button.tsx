import Button from "@mui/material/Button";

const ContainedButton = ({
  text,
  handleOnClick,
}: {
  text: string;
  handleOnClick: () => void;
}) => (
  <Button variant="contained" onClick={handleOnClick}>
    {text}
  </Button>
);

const OutlinedButton = ({
  text,
  handleOnClick,
}: {
  text: string;
  handleOnClick: () => void;
}) => (
  <Button variant="outlined" onClick={handleOnClick}>
    {text}
  </Button>
);

export { ContainedButton, OutlinedButton };

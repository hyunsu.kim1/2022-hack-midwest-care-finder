import { List } from "@mui/material";
import { useMapProxy } from "../services/hooks/use-map-proxy";
import parseClinicCardInfo from "../services/utils/parseClinicCardInfo";
import parseStateCardInfo from "../services/utils/parseStateCardInfo";
import { ClinicDetailsType, StateRestrictionType } from "../type/types";
import ClinicListCard from "./clinicListCard";
import StateListCard from "./stateListCard";

interface ListContainerProps {
  height: string;
  type: string;
  stateRestrictions?: StateRestrictionType[];
}

const ListContainer = (props: ListContainerProps) => {
  const { height, type, stateRestrictions } = props;

  const { data, isLoading } = useMapProxy(
    "abortion_results",
    `abortion&nbsp;clinic`
  );

  const clinicDetails: ClinicDetailsType[] = parseClinicCardInfo(data);

  return (
    <List
      sx={{
        width: "100%",
        bgcolor: "background.paper",
        position: "relative",
        overflow: "auto",
        height: height,
      }}
    >
      {type === "clinic"
        ? clinicDetails.map((details, index) => (
            <ClinicListCard key={index} details={details} />
          ))
        : stateRestrictions?.map((details, index) => (
            <StateListCard key={index} details={details} />
          ))}
    </List>
  );
};

export default ListContainer;

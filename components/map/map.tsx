import React from "react";
import {
  GoogleMap,
  useLoadScript,
  Marker,
  InfoWindow,
} from "@react-google-maps/api";
import Box from "@mui/material/Box";

interface MapProps {
  containerStyle: React.CSSProperties;
  clinicInfos: { location: { lat: number; lng: number }; name: string }[];
  zoom: number;
}

function Map(props: MapProps) {
  const [map, setMap] = React.useState<google.maps.Map | null>(null);
  const [activeMarker, setActiveMarker] = React.useState<string | null>(null);

  const handleActiveMarker = (marker: string) => {
    if (marker === activeMarker) {
      return;
    }
    setActiveMarker(marker);
  };

  const onMapLoad = React.useCallback((map: google.maps.Map) => {
    console.log("map.data: ", map.data);
    // map.data.loadGeoJson('/geo.json')
    setMap(map);
  }, []);

  const onClick = (...args: any[]) => {
    console.log("onClick args: ", args);
  };

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey:
      process.env.GOOGLE_MAPS_API_KEY ??
      "AIzaSyC27d6LKXgZl2oxPYEkaS3-6kld1QutGGg",
  });

  React.useEffect(() => {
    console.log("map load error: ", loadError);
  }, [loadError]);

  return isLoaded ? (
    <GoogleMap
      mapContainerStyle={props.containerStyle}
      center={props?.clinicInfos[0]?.location}
      zoom={props.zoom}
      onLoad={onMapLoad}
      onClick={onClick}
    >
      {props.clinicInfos.map((info) => (
        <Marker
          key={`${info.location.lat},${info.location.lng}`}
          position={info.location}
          onClick={() =>
            handleActiveMarker(`${info.location.lat},${info.location.lng}`)
          }
        >
          {activeMarker === `${info.location.lat},${info.location.lng}` ? (
            <InfoWindow onCloseClick={() => setActiveMarker(null)}>
              <div>{info.name}</div>
            </InfoWindow>
          ) : null}
        </Marker>
      ))}
    </GoogleMap>
  ) : (
    <Box>Loading...</Box>
  );
}

export default React.memo(Map);

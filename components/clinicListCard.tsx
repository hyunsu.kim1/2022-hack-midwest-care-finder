import { Paper, CardContent, ListItemButton, Typography } from "@mui/material";
import { ClinicDetailsType } from "../type/types";

interface ListCardProps {
  details: ClinicDetailsType;
}

const ClinicListCard = (props: ListCardProps) => {
  const { details } = props;

  return (
    <Paper elevation={5} sx={{ margin: 1, borderRadius: "25px" }}>
      <ListItemButton
        sx={{
          minWidth: 275,
          borderRadius: "25px",
          mb: 2
        }}
      >
        <CardContent>
          <>
            <Typography variant="h5" component="div">
              {details.name}
            </Typography>
            <Typography
              sx={{ fontSize: 14 }}
              color="text.secondary"
              gutterBottom
            >
              {details.address}
            </Typography>
            <Typography variant="body2">
              Open Now: {details.openNow ? "Yes" : "No"}
            </Typography>
          </>
        </CardContent>
      </ListItemButton>
    </Paper>
  );
};

export default ClinicListCard;

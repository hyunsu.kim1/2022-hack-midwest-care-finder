import { FieldProps, getIn } from "formik";
import { TextField, TextFieldProps as MuiTextFieldProps } from "@mui/material";

interface TextFieldProps
  extends FieldProps,
    Omit<MuiTextFieldProps, "name" | "value" | "error"> {}

const OutlinedFormikTextField = ({ children, ...props }: TextFieldProps) => {
  const {
    disabled,
    field: { onBlur: fieldOnBlur, ...field },
    form: { isSubmitting, touched, errors },
    onBlur,
    helperText,
    ...rest
  } = props;

  const fieldError = getIn(errors, field.name);
  const showError = getIn(touched, field.name) && !!fieldError;

  return (
    <TextField
      variant="outlined"
      error={showError}
      helperText={showError ? fieldError : helperText}
      disabled={disabled ?? isSubmitting}
      onBlur={
        onBlur ??
        function (e) {
          fieldOnBlur(e ?? field.name);
        }
      }
      {...field}
      {...rest}
    >
      {children}
    </TextField>
  );
};

export default OutlinedFormikTextField;

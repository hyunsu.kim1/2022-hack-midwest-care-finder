import { Checkbox, CheckboxProps as MuiCheckboxProps } from "@mui/material";
import { FieldProps } from "formik";

interface CheckboxProps
  extends FieldProps,
    Omit<
      MuiCheckboxProps,
      | "name"
      | "value"
      | "error"
      | "form"
      | "checked"
      | "defaultChecked"
      // Excluded for conflict with Field type
      | "type"
    > {
  type?: string;
}

const FormikCheckbox = (props: CheckboxProps) => {
  const {
    disabled,
    field: { onBlur: fieldOnBlur, ...field },
    form: { isSubmitting },
    type,
    onBlur,
    ...rest
  } = props;

  const indeterminate = !Array.isArray(field.value) && field.value == null;

  return (
    <Checkbox
      disabled={disabled ?? isSubmitting}
      indeterminate={indeterminate}
      onBlur={
        onBlur ??
        function (e) {
          fieldOnBlur(e ?? field.name);
        }
      }
      {...field}
      {...rest}
    />
  );
};

export default FormikCheckbox;

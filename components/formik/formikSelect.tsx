import { getIn, FieldProps } from "formik";
import {
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  FormHelperText,
} from "@mui/material";
import { StateOptionType } from "../../type/types";

interface OutlinedFormikSelectProps extends FieldProps {
  label: string;
  name: string;
  options: StateOptionType[];
  value?: string;
  width?: string;
  required?: boolean;
}

const OutlinedFormikSelect = (props: OutlinedFormikSelectProps) => {
  const {
    label,
    name,
    options,
    value,
    width,
    required = false,
    field,
    form,
    ...rest
  } = props;

  const isTouched = getIn(form.touched, field.name);
  const errorMessage = getIn(form.errors, field.name);

  return (
    <FormControl
      variant="outlined"
      style={{ width: "100%" }}
      required={required}
      error={Boolean(isTouched && errorMessage)}
    >
      <InputLabel id={`label-id-${name}`}>{label}</InputLabel>
      <Select labelId={`label-id-${name}`} label={label} {...rest} {...field}>
        {options?.map((option) => (
          <MenuItem value={option.optionLabel} key={option.id}>
            {option.optionLabel}
          </MenuItem>
        ))}
      </Select>
      <FormHelperText>
        {isTouched && errorMessage ? errorMessage : undefined}
      </FormHelperText>
    </FormControl>
  );
};

export default OutlinedFormikSelect;

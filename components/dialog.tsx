import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  Button,
  DialogActions,
} from "@mui/material";

export interface TextDialogProps {
  open: boolean;
  onClose: () => void;
}
const TextDialog = (props: TextDialogProps) => {
  const { open, onClose } = props;

  return (
    <Dialog onClose={onClose} open={open}>
      <DialogTitle>Terms and Conditions</DialogTitle>
      <DialogContent>
        <DialogContentText id="terms-and-condition">
          Limitations on Liability In no event will Care Finder App be liable
          with to respect to any subject matter of this Agreement under any
          contract, negligence, strict liability, or other legal or equitable
          theory for: (1) any special, incidental, or consequential damages; (2)
          the cost of procurement of substitute products or services; or (3) for
          interruption of use or loss or corruption of data. General
          Representations You hereby warrant that (1) your use of the services
          will be in strict accordance with this Agreement, and all applicable
          laws and regulations, and (2) your use of the services will not
          infringe or misappropriate the intellectual property rights of any
          third party. Indemnification You agree to indemnify and hold harmless
          Care Finder App, its contractors, employees, agents, and the like from
          and against any and all claims and expenses including attorney's fees,
          arising out of your use of the APIs, including but not limited to
          violation of this Agreement. This Indemnification clause shall not
          apply to government entities. Miscellaneous This Agreement constitutes
          the entire Agreement between Care Finder App and you concerning the
          subject matter hereof, and may only be modified by the posting of a
          revised version on this page by Care Finder App. Disputes Any disputes
          arising out of this Agreement and access to or use of the services
          shall be governed, interpreted, and enforced in accordance with the
          federal laws of the United States of America. No Waiver of Rights Care
          Finder App's failure to exercise or enforce any right or provision of
          this Agreement shall not constitute waiver of such right or provision.{" "}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
};

export default TextDialog;

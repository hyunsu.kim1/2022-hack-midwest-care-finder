import React from "react";
import { Paper, Box, Grid, Fade, Button, Tabs, Tab } from "@mui/material";
import FilterListIcon from "@mui/icons-material/FilterList";

import { useProxy } from "../services/hooks/use-proxy";
import FilterForm from "./filterForm";
import ListContainer from "./listContainer";
import { FormValues } from "../type/types";

const SearchBox = () => {
  const [openFilter, setOpenFilter] = React.useState(true);
  const [tabValue, setTabValue] = React.useState<number>(0);
  const [payload, setPayload] = React.useState<FormValues>({
    weeks_since_lmp: null,
    states: [],
    age: null,
  });

  const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  const { data, isLoading, refetch } = useProxy(`policyData`, payload);

  React.useEffect(() => {
    refetch();
  }, [payload]);

  const filterHeight = openFilter ? "550px" : "0px";
  const resultHeight = openFilter
    ? `calc(85vh - ${filterHeight})`
    : "calc(95vh - 100px)";

  const handleOpenFilter = () => {
    setOpenFilter(!openFilter);
  };

  console.log(payload);

  const handleSearch = (values: any) => {
    setOpenFilter(false);
    setPayload({
      weeks_since_lmp: values.weeks_since_lmp,
      states: values.states,
      age: values.age,
    });
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="flex-start"
      alignItems="flex-start"
      spacing={2}
    >
      <Grid item xs={12} m={3} height={filterHeight}>
        <Button
          variant={openFilter ? "contained" : "outlined"}
          startIcon={<FilterListIcon />}
          onClick={handleOpenFilter}
        >
          Filter
        </Button>
        <Fade in={openFilter}>
          <Box pt={2}>
            <Paper elevation={5} sx={{ padding: "30px", borderRadius: "25px" }}>
              <FilterForm handleOnSubmit={handleSearch} />
            </Paper>
          </Box>
        </Fade>
      </Grid>
      <Grid item xs>
        <Tabs value={tabValue} onChange={handleTabChange}>
          <Tab label="State Restrictions" />
          <Tab label="Clinics Found" />
        </Tabs>
        <TabPanel value={tabValue} index={0}>
          {tabValue === 0 ? (
            <ListContainer height={resultHeight} type="state" />
          ) : null}
        </TabPanel>
        <TabPanel value={tabValue} index={1}>
          {tabValue === 1 ? (
            <ListContainer
              height={resultHeight}
              type="clinic"
              stateRestrictions={data}
            />
          ) : null}
        </TabPanel>
      </Grid>
    </Grid>
  );
};
interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

const TabPanel = (props: TabPanelProps) => {
  const { children, value, index } = props;

  return <Box>{value === index && <Box sx={{ p: 3 }}>{children}</Box>}</Box>;
};

export default SearchBox;

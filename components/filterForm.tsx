import React from "react";
import { Field, Form, Formik } from "formik";
import * as yup from "yup";
import {
  Box,
  Button,
  Chip,
  FormControlLabel,
  Grid,
  FormHelperText,
  FormControl,
  Typography,
} from "@mui/material";
import FormikCheckbox from "./formik/formikCheckbox";
import OutlinedFormikSelect from "./formik/formikSelect";
import OutlinedFormikTextField from "./formik/formikTextField";
import { stateOptions } from "../options/stateOptions";
import TextDialog from "./dialog";

interface FilterFormProps {
  handleOnSubmit: (data: any) => void;
}

const FilterForm = (props: FilterFormProps) => {
  const { handleOnSubmit } = props;
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  const initialValues = {
    address: "",
    weeks_since_lmp: null,
    states: [] as string[],
    stateLabels: [] as string[],
    age: null,
    terms: false,
  };

  const validationSchema = yup.object().shape({
    address: yup.string(),
    weeks_since_lmp: yup.number().nullable(),
    states: yup.array().of(yup.string()).required("Required"),
    age: yup.number().nullable(),
    terms: yup
      .boolean()
      .oneOf([true], "Message")
      .required("You must accept the terms and conditions."), //required to be true
  });

  const termsLabel = (
    <Typography variant="body2" display="block" gutterBottom>
      I accept the
      <Button onClick={handleClickOpen}>terms and conditions.</Button>
    </Typography>
  );

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={(values) => {
        handleOnSubmit(values);
      }}
    >
      {({ values, handleSubmit }) => {
        return (
          <Form onSubmit={handleSubmit}>
            <Grid
              container
              direction="row"
              justifyContent="space-around"
              alignItems="center"
              spacing={2}
            >
              <Grid item xs={12}>
                <Field
                  component={OutlinedFormikTextField}
                  label="Search Address"
                  name="address"
                  variant="outlined"
                  fullWidth
                  value={values.address}
                />
              </Grid>

              <Grid item xs={12}>
                <Field
                  type="number"
                  component={OutlinedFormikTextField}
                  name="weeks_since_lmp"
                  label="Week since last menstrual period"
                  variant="outlined"
                  fullWidth
                  value={values.weeks_since_lmp || ""}
                />
              </Grid>

              <Grid item xs={12}>
                <Field
                  component={OutlinedFormikSelect}
                  required
                  multiple
                  name="states"
                  label="State(s)"
                  options={stateOptions}
                  value={values.states}
                  renderValue={(selected: string[]) => (
                    <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                      {selected.map((value: string) => (
                        <Chip key={value} label={value} />
                      ))}
                    </Box>
                  )}
                  MenuProps={MenuProps}
                />
              </Grid>

              <Grid item xs={12}>
                <Field
                  type="number"
                  component={OutlinedFormikTextField}
                  label="Age"
                  name="age"
                  variant="outlined"
                  fullWidth
                  value={values.age || ""}
                />
              </Grid>

              <Grid item xs={12}>
                <FormControl required error={!values.terms}>
                  <FormControlLabel
                    control={
                      <Field
                        component={FormikCheckbox}
                        color="primary"
                        type="checkbox"
                        name="terms"
                        value={values.terms}
                        checked={values.terms}
                      />
                    }
                    label={termsLabel}
                  />
                  <TextDialog open={open} onClose={handleClose} />
                  <FormHelperText>
                    You must agree to the terms and conditions in order to use
                    this app.
                  </FormHelperText>
                </FormControl>
              </Grid>

              <Grid item xs={12}>
                <Button fullWidth variant="contained" type="submit">
                  Search
                </Button>
              </Grid>
            </Grid>
          </Form>
        );
      }}
    </Formik>
  );
};

export default FilterForm;

export type SelectOptionType = {
  id: string | number;
  optionLabel: string | number;
  dbValue: string | number;
};

export type StateOptionType = {
  id: number;
  optionLabel: string;
  coordinates: {
    lat: number;
    lng: number;
  };
};

export type StateRestrictionType = {
  state?: string;
  weeksSinceLmp?: number;
  waitingPeriodPostCounselingHours?: number;
  waitingPeriodPostCounselingNotes?: string;
  counselingVisitCount?: number;
  counselingException?: string;
  requiresCoverage?: boolean;
  privateExceptionLife?: boolean;
  privateExceptionHealth?: string;
  privateExceptionRapeOrIncest?: boolean;
  exchangeCoverageNoRestrictions?: boolean;
  exchangeExceptionLife?: boolean;
  exchangeExceptionHealth?: string;
  exchangeExceptionFetal?: string;
  exchangeExceptionRapeOrIncest?: boolean;
  exchangeForbidsCoverage?: boolean;
  medicaidCoverageProviderPatientDecision?: boolean;
  medicaidExceptionLife?: boolean;
  medicaidExceptionHealth?: string;
  medicaidExceptionFetal?: string;
  medicaidExceptionRapeOrIncest?: boolean;
  belowAge?: number;
  parentalConsentRequired?: boolean;
  parentalNotificationRequired?: boolean;
  parentsRequired?: number;
  judicialBypassAvailable?: boolean;
  allowsMinorToConsent?: boolean;
};

export type ClinicDetailsType = {
  name: string;
  address: string;
  openNow: boolean;
};

export type MapResultPhoto = {
  height: number;
  html_attributions: string[];
  photo_reference: string;
  width: number;
};

export type MapResult = {
  business_status: string;
  formatted_address: string;
  geometry: {
    location: {
      lat: number;
      lng: number;
    };
    viewport: {
      northeast: {
        lat: number;
        lng: number;
      };
      southwest: {
        lat: number;
        lng: number;
      };
    };
  };
  icon: string;
  icon_background_color: string;
  icon_mask_base_uri: string;
  name: string;
  opening_hours: {
    open_now: boolean;
  };
  photos: MapResultPhoto[];
  place_id: string;
  plus_code: {
    compound_code: string;
    global_code: string;
  };
  rating: number;
  reference: string;
  types: string[];
  user_ratings_total: number;
};

export type FormValues = {
  weeks_since_lmp: number | null;
  states?: string[];
  age: number | null;
};

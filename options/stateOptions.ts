import { StateOptionType } from "../type/types";

export const stateOptions: StateOptionType[] = [
  {
    id: 1,
    optionLabel: "Alabama",
    coordinates: { lat: 32.806671, lng: -86.79113 },
  },
  {
    id: 2,
    optionLabel: "Alaska",
    coordinates: { lat: 61.370716, lng: -152.404419 },
  },
  {
    id: 3,
    optionLabel: "Arizona",
    coordinates: { lat: 33.729759, lng: -111.431221 },
  },
  {
    id: 4,
    optionLabel: "Arkansas",
    coordinates: { lat: 34.969704, lng: -92.373123 },
  },
  {
    id: 5,
    optionLabel: "California",
    coordinates: { lat: 36.116203, lng: -119.681564 },
  },
  {
    id: 6,
    optionLabel: "Colorado",
    coordinates: { lat: 39.059811, lng: -105.311104 },
  },
  {
    id: 7,
    optionLabel: "Connecticut",
    coordinates: { lat: 41.82355, lng: -72.06549 },
  },
  {
    id: 8,
    optionLabel: "Delaware",
    coordinates: { lat: 39.318523, lng: -75.507141 },
  },
  {
    id: 9,
    optionLabel: "Florida",
    coordinates: { lat: 27.766279, lng: -81.686783 },
  },
  {
    id: 10,
    optionLabel: "Georgia",
    coordinates: { lat: 33.040619, lng: -83.643074 },
  },
  {
    id: 11,
    optionLabel: "Hawaii",
    coordinates: { lat: 21.094318, lng: -157.498337 },
  },
  {
    id: 12,
    optionLabel: "Idaho",
    coordinates: { lat: 44.240459, lng: -114.478828 },
  },
  {
    id: 13,
    optionLabel: "Illinois",
    coordinates: { lat: 40.633125, lng: -89.690546 },
  },
  {
    id: 14,
    optionLabel: "Indiana",
    coordinates: { lat: 39.864727, lng: -86.260376 },
  },
  {
    id: 15,
    optionLabel: "Iowa",
    coordinates: { lat: 41.977856, lng: -93.097702 },
  },
  {
    id: 16,
    optionLabel: "Kansas",
    coordinates: { lat: 39.011902, lng: -95.72084 },
  },
  {
    id: 17,
    optionLabel: "Kentucky",
    coordinates: { lat: 37.839333, lng: -84.270018 },
  },
  {
    id: 18,
    optionLabel: "Louisiana",
    coordinates: { lat: 31.126815, lng: -91.867805 },
  },
  {
    id: 19,
    optionLabel: "Maine",
    coordinates: { lat: 44.323535, lng: -69.765261 },
  },
  {
    id: 20,
    optionLabel: "Maryland",
    coordinates: { lat: 39.045755, lng: -76.641273 },
  },
  {
    id: 21,
    optionLabel: "Massachusetts",
    coordinates: { lat: 42.407211, lng: -71.382439 },
  },
  {
    id: 22,
    optionLabel: "Michigan",
    coordinates: { lat: 43.326618, lng: -84.532215 },
  },
  {
    id: 23,
    optionLabel: "Minnesota",
    coordinates: { lat: 46.729553, lng: -94.6859 },
  },
  {
    id: 24,
    optionLabel: "Mississippi",
    coordinates: { lat: 32.741646, lng: -89.678696 },
  },
  {
    id: 25,
    optionLabel: "Missouri",
    coordinates: { lat: 38.456085, lng: -92.288368 },
  },
  {
    id: 26,
    optionLabel: "Montana",
    coordinates: { lat: 46.879682, lng: -110.362565 },
  },
  {
    id: 27,
    optionLabel: "Nebraska",
    coordinates: { lat: 41.125329, lng: -98.268082 },
  },
  {
    id: 28,
    optionLabel: "Nevada",
    coordinates: { lat: 38.313515, lng: -117.055374 },
  },
  {
    id: 29,
    optionLabel: "New Hampshire",
    coordinates: { lat: 43.452492, lng: -71.563896 },
  },
  {
    id: 30,
    optionLabel: "New Jersey",
    coordinates: { lat: 40.298904, lng: -74.521011 },
  },
  {
    id: 31,
    optionLabel: "New Mexico",
    coordinates: { lat: 34.840515, lng: -106.248482 },
  },
  {
    id: 32,
    optionLabel: "New York",
    coordinates: { lat: 42.165726, lng: -74.948051 },
  },
  {
    id: 33,
    optionLabel: "North Carolina",
    coordinates: { lat: 35.630066, lng: -79.806419 },
  },
  {
    id: 34,
    optionLabel: "North Dakota",
    coordinates: { lat: 47.551493, lng: -99.784012 },
  },
  {
    id: 35,
    optionLabel: "Ohio",
    coordinates: { lat: 40.388783, lng: -82.764915 },
  },
  {
    id: 36,
    optionLabel: "Oklahoma",
    coordinates: { lat: 35.007752, lng: -97.092877 },
  },
  {
    id: 37,
    optionLabel: "Oregon",
    coordinates: { lat: 44.124297, lng: -120.680254 },
  },
  {
    id: 38,
    optionLabel: "Pennsylvania",
    coordinates: { lat: 40.590752, lng: -77.209755 },
  },
  {
    id: 39,
    optionLabel: "Rhode Island",
    coordinates: { lat: 41.82355, lng: -72.06549 },
  },
  {
    id: 40,
    optionLabel: "South Carolina",
    coordinates: { lat: 33.856892, lng: -80.945007 },
  },
  {
    id: 41,
    optionLabel: "South Dakota",
    coordinates: { lat: 44.323535, lng: -69.765261 },
  },
  {
    id: 42,
    optionLabel: "Tennessee",
    coordinates: { lat: 35.830521, lng: -86.39027 },
  },
  {
    id: 43,
    optionLabel: "Texas",
    coordinates: { lat: 31.968599, lng: -99.901813 },
  },
  {
    id: 44,
    optionLabel: "Utah",
    coordinates: { lat: 40.150032, lng: -111.862434 },
  },
  {
    id: 45,
    optionLabel: "Vermont",
    coordinates: { lat: 44.045876, lng: -72.710686 },
  },
  {
    id: 46,
    optionLabel: "Virginia",
    coordinates: { lat: 37.769337, lng: -78.169968 },
  },
  {
    id: 47,
    optionLabel: "Washington",
    coordinates: { lat: 47.751074, lng: -120.740139 },
  },
  {
    id: 48,
    optionLabel: "West Virginia",
    coordinates: { lat: 38.468097, lng: -80.945007 },
  },
  {
    id: 49,
    optionLabel: "Wisconsin",
    coordinates: { lat: 44.268543, lng: -89.616508 },
  },
  {
    id: 50,
    optionLabel: "Wyoming",
    coordinates: { lat: 42.755966, lng: -107.30249 },
  },
  {
    id: 51,
    optionLabel: "District Of Columbia",
    coordinates: { lat: 38.9072, lng: -77.0369 },
  },
];

import React, { useContext, createContext } from "react";

const LocationContext = createContext({
  state: "",
  coordinates: { lat: "", lng: "" },
});

const LocationProvider = ({ children }: any) => {
  const [state, setState] = React.useState("");
  const [coordinates, setCoordinates] = React.useState({ lat: "", lng: "" });

  const value = {
    state,
    setState,
    coordinates,
    setCoordinates,
  };

  return (
    <LocationContext.Provider value={value}>
      {children}
    </LocationContext.Provider>
  );
};

const useLocationContext = () => useContext(LocationContext);

export { LocationContext, LocationProvider, useLocationContext };

import axios from "axios";

export const serverClient = axios.create({
  timeout: 10000,
  baseURL: "https://rocky-castle-58391.herokuapp.com/",
});

import { useQuery } from "@tanstack/react-query";

import { serverClient } from "../baseClient";
import { FormValues, StateRestrictionType } from "../../type/types";

export const useProxy = (queryKey: string, payload: FormValues) => {
  const getProxiedData = async () => {
    const { data, status } = await serverClient.post("/restrictions/state", {
      weeksSinceLmp: payload.weeks_since_lmp,
      states: payload.states,
      age: payload.age,
    });

    if (status !== 200) {
      console.error("error");
    }
    return data;
  };

  return useQuery<StateRestrictionType[]>([`${queryKey}`], getProxiedData);
};

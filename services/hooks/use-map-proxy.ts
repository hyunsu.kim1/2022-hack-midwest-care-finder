import { serverClient } from "../baseClient";
import { useQuery } from "@tanstack/react-query";

export const useMapProxy = (queryKey: string, url: string) => {
  const getProxiedData = async () => {
    const { data, status } = await serverClient.post("/mapproxy", {
      url: url,
    });

    if (status !== 200) {
      console.error("error");
    }
    return data;
  };

  return useQuery([`${queryKey}`], getProxiedData);
};

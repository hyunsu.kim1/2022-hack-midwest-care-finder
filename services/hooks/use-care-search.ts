import { useQuery } from "@tanstack/react-query";
import { serverClient } from "../baseClient";

export const useCareSearch = (formValues: any) => {
  const getCareSearch = async () => {
    const { data, status } = await serverClient.post(
      "/staterestrictions",
      formValues
    );

    if (status !== 200) {
      console.error("error staterestrictions");
    }
    return data;
  };
  return useQuery(["stateRestrictions"], getCareSearch);
};

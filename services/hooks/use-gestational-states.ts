import { serverClient } from "../baseClient";
import { useQuery } from "@tanstack/react-query";

/** DEPRECATED DO NOT USE LOL
 * PLZ USE THE USE PROXY HOOK IN THIS SAME DIRECTORY
 * NERD
 */
export const useGestationalStates = () => {
  const fetchGestationalStates = async () => {
    const { data, status } = await serverClient.post("/proxy", {
      url: "/waiting_periods/zips/30313",
    });

    if (status !== 200) {
        console.error('error')
    }
    return data;
  };
  return useQuery(["gestational-states"], fetchGestationalStates);
};

import { MapResult, ClinicDetailsType } from "../../type/types";

const parseClinicCardInfo = (data: any): ClinicDetailsType[] => {
  return data?.results.map((result: MapResult) => {
    return {
      name: result.name,
      address: result.formatted_address,
      openNow: result.opening_hours ? result.opening_hours.open_now : false,
    };
  });
};

export default parseClinicCardInfo;

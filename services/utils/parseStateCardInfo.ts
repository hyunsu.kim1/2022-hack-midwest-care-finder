import { MapResult, StateRestrictionType } from "../../type/types";

const parseStateCardInfo = (data: any) => {
  let parsedResult: StateRestrictionType[] = [];
  let stateList: string[] = [];

  data?.results.map((result: MapResult) => {
    const cityStateName = result.plus_code.compound_code.slice(8);
    const commaIndex = cityStateName.indexOf(",");
    const stateName = cityStateName.slice(commaIndex + 1);

    if (!stateList.includes(stateName)) {
      stateList.push(stateName);
    }
  });

  for (let i = 0; i < stateList?.length; i++) {
    parsedResult[i] = { state: stateList[i] };
  }

  // TODO: map through result array and pull policy data for each state

  return parsedResult;
};

// fullName: "Alabama",
// parentalInvolvementRequiredCondition: "Under 18",
// publicFundingCondition: "Medicaid",
// limitedPrivateInsurance: true,
// performedByLicensedPhysician: true,
// performedAtHospitalCondition: "Licensed doctors only",
// secondPhysicianRequriedCondition: "Over 12 weeks",
// partialBirthBannedCondition: "Over 18 weeks",
// waitingPeriodPostCounselingHours: 12,
// mandatedCounselingBreastCancerLink: true,
// mandatedCounselingBreastFetalPain: true,
// mandatedCousnelingBreastnegativPsychEffects: true,

export default parseStateCardInfo;
